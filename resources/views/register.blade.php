<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width. initial-scale=1.0">
	<title>Form</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>
	<h2>Sign Up Form</h2>
	<form action="welcome" method="POST">
		@csrf
		<label>First Name:</label> <br><br>
		<input type="text" name="first_name"> <br><br>
		<label>Last Name:</label> <br><br>
		<input type="text" name="last_name"> <br><br>
		<label>Gender:</label> <br><br>
		<input type="radio" name="Gender">Male <br>
		<input type="radio" name="Gender">Female <br>
		<input type="radio" name="Gender">Other <br> <br>
		<label>Nationality:</label> <br><br>
		<select name="Nation"> 
			<option value="Indonesia">Indonesia</option>
			<option value="Singapura">Singapura</option>
			<option value="Malaysia">Malaysia</option>
			<option value="Australia">Australia</option>
		</select> <br><br>
		<label>Language Spoken:</label> <br><br>
		<input type="checkbox"> Bahasa Indonesia <br>
		<input type="checkbox"> English <br>
		<input type="checkbox"> Other <br> <br>
		<label>Bio:</label> <br><br>
		<textarea name="bio" cols="30" rows="10"></textarea> <br><br>
		<input type="Submit" value="Sign Up">
	</form>

</body>
</html>